from django.urls import path

from register import views

urlpatterns = [
    path('register/', views.UserExtendCreateView.as_view(), name='register')
]