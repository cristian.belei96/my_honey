from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import TextInput, EmailInput, CheckboxInput


from register.models import UserExtend


class UserExtendForm(UserCreationForm):
    class Meta:
        model = UserExtend
        fields = ['first_name', 'last_name', 'email', 'phone', 'region', 'address', 'GDPR_consent']
        widgets = {
            'first_name': TextInput(attrs={'placeholder': 'Nume', 'class':'form-control'}),
            'last_name': TextInput(attrs={'placeholder': 'Prenume', 'class':'form-control'}),
            'username': TextInput(attrs={'placeholder': 'Enter your name', 'class': 'form-control'}),
            'email': EmailInput(attrs={'placeholder': 'Email-ul', 'class':'form-control'}),
            'phone': TextInput(attrs={'placeholder': 'Numarul de telefon', 'class':'form-control'}),
            'region': TextInput(attrs={'placeholder': 'Judet', 'class': 'form-control'}),
            'address': TextInput(attrs={'placeholder': 'Adresa completa', 'class':'form-control'}),
            'GDPR_consent': CheckboxInput(attrs={'required': ''}),
        }

    def __init__(self, *args, **kwargs):
        super(UserExtendForm, self).__init__(*args, **kwargs)
        self.fields['password1'].widget.attrs['class'] = 'form-control'
        self.fields['password1'].widget.attrs['placeholder'] = 'Parola'

        self.fields['password2'].widget.attrs['class'] = 'form-control'
        self.fields['password2'].widget.attrs['placeholder'] = 'Confirmare parola'

