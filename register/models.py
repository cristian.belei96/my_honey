from django.contrib.auth.models import User
from django.db import models


class UserExtend(User):
    phone = models.CharField(max_length=15)
    region = models.CharField(max_length=100, null=True, blank=False)
    address = models.CharField(max_length=250, null=False, blank=False)
    GDPR_consent = models.BooleanField(default=False)

    def __str__(self):
        return self.first_name
