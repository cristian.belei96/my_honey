from django.http import HttpResponse
from django.views.generic import TemplateView


def index(request):
    return HttpResponse


class HomeTemplateView(TemplateView):
    template_name = 'home/homepage.html'
