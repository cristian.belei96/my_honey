from django.urls import path

from category import views

urlpatterns =[
    path('category/<int:pk>/',views.CategoryDetailView.as_view(), name='category_details')

]