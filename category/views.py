from django.shortcuts import render
from django.views.generic import DetailView

from category.models import Category
from products.models import Product


class CategoryDetailView(DetailView):
    template_name = 'products/products.html'
    model = Category
    context_object_name = 'category'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        category = context[self.context_object_name]
        products = Product.objects.filter(category=category)
        context['products'] = products
        return context
