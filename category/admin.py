from django.contrib import admin
from category.models import Category
from products.models import *

admin.site.register(Category)
admin.site.register(Product)
admin.site.register(Cart)
admin.site.register(CartItem)
admin.site.register(Packaging)
