from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView

from category.models import Category
from products.models import Product, Cart, CartItem


class ProductListView(ListView):
    template_name = 'products/products.html'
    model = Product
    context_object_name = 'products'


class ProductDetailView(DetailView):
    template_name = 'products/product.html'
    model = Product
    context_object_name = 'product'


def get_products_per_category(request, pk):
    products = Product.objects.filter(category_id=pk)
    get_category = Category.objects.get(id=pk)
    return render(request, 'products/products.html', {'products': products, 'name_of_category': get_category})


def get_open_cart(request):
    open_carts = Cart.objects.filter(user=request.user, status='open')
    if open_carts.count() > 0:
        return open_carts.first()
    else:
        return Cart.objects.create(user=request.user, status='open')


@login_required
def add_product_to_cart(request):
    if request.method == 'POST':
        product_id = request.POST.get('product_id')
        quantity = int(request.POST.get('quantity', 1))
        cart = get_open_cart(request)
        existing_cart_items = CartItem.objects.filter(cart=cart, product_id=product_id)
        if existing_cart_items.count() > 0:
            cart_item = existing_cart_items.first()
            q = cart_item.quantity
            q += quantity
            cart_item.quantity = q
            cart_item.save()
        else:
            CartItem.objects.create(cart=cart, product_id=product_id, quantity=quantity)
        return redirect(request.META['HTTP_REFERER'])


@login_required()
def open_cart_view(request):
    if request.method == 'POST':
        cart_item_id = request.POST.get('cart_item_id')
        quantity = request.POST.get('quantity')
        cart_item = CartItem.objects.get(id=cart_item_id)
        cart_item.quantity = quantity
        cart_item.save()
    cart = get_open_cart(request)
    cart_items = CartItem.objects.filter(cart=cart)
    return render(request, 'products/open_cart.html', {'cart_items': cart_items, 'cart': cart})



@login_required()
def delete_cart_item(request, pk):
    current_cart_item = CartItem.objects.filter(id=pk)
    current_cart_item.delete()
    return redirect('open_cart_view')


# def cart_details(request):
#     order = Order(request)
#     return render(request, 'products/open_cart.html', {'order': order, })


@login_required()
def checkout_view(request):
    cart = get_open_cart(request)
    cart_items = CartItem.objects.filter(cart=cart)
    for cart_item in cart_items:
        product = cart_item.product
        product.stock -= cart_item.quantity
        product.save()
    cart.status = 'close'
    cart.save()
    return redirect(reverse_lazy('homepage'))




