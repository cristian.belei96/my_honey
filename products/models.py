from django.contrib.auth.models import User
from django.db import models

from category.models import Category


class Packaging(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class Product(models.Model):
    stock_choices = (('yes', 'Yes'), ('no', 'No'))
    name = models.CharField(max_length=100, null=True, blank=True)
    description = models.TextField(max_length=250, null=True, blank=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    is_stock = models.CharField(max_length=50, choices=stock_choices, default="yes")
    price = models.DecimalField(max_digits=10, decimal_places=2)
    image = models.ImageField(upload_to='static/img/products/')
    packagings = models.ManyToManyField(Packaging, blank=True)
    stock = models.IntegerField(default=1)

    def __str__(self):
        return self.name


class Cart(models.Model):
    STATUES = (('open', 'Open'),
               ('close', 'Close'),)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.CharField(max_length=25, choices=STATUES)

    def __str__(self):
        return f'{self.status} cart of {self.user}'


class CartItem(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1)
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.quantity} x {self.product.name} in {self.cart}'


