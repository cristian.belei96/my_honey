from django.contrib.auth.forms import UserCreationForm, AuthenticationForm

from category.models import Category
from products.models import Product, CartItem
from products.views import get_open_cart
from register.forms import UserExtendForm


def navbar_data(request):
    return {'categories': Category.objects.all()}


def common_data(request):
    if request.user.is_authenticated:
        cart = get_open_cart(request)
        cart_items = CartItem.objects.filter(cart=cart)
        count = sum([c.quantity for c in cart_items])
    else:
        count = 0
        cart_items = []
    user_extend_form = UserExtendForm()
    context = {
        'user_extend_form': user_extend_form,
        'user_login_form': AuthenticationForm(),
        'products': Product.objects.all(),
        'cart_count': count,
        'cart_items': cart_items,
        'categories': Category.objects.all(),
        'all_categories': Category.objects.all(),
    }
    return context

