from django.urls import path
from products import views

urlpatterns =[
    path('products-page/', views.ProductListView.as_view(), name='products_page'),
    path('product-page/<int:pk>/', views.ProductDetailView.as_view(), name='product_page'),
    path('products-per-category/<int:pk>/', views.get_products_per_category, name='products_per_category'),
    path('add-product-to-cart/', views.add_product_to_cart, name='add_product_to_cart'),
    path('open-cart-view/', views.open_cart_view, name='open_cart_view'),
    path('delete-cart-item/<int:pk>', views.delete_cart_item, name='delete_cart_item'),
    path('checkout/', views.checkout_view, name='checkout')
]

